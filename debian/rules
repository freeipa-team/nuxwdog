#!/usr/bin/make -f
# See debhelper(7) (uncomment to enable)
# output every command that modifies files on the build system.
#DH_VERBOSE = 1

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/default.mk

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

VERSION = $(shell dpkg-parsechangelog -S Version | sed 's/-.*//')

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -q DEB_HOST_MULTIARCH)
DEB_HOST_ARCH_BITS ?= $(shell dpkg-architecture -q DEB_HOST_ARCH_BITS)

ant_flags = -Dproduct.ui.flavor.prefix="" \
		-Dproduct.prefix="" \
		-Dproduct="nuxwdog" \
		-Dversion="${VERSION}"

confflags =

ifeq ($(DEB_HOST_ARCH_BITS), 64)
	confflags += --enable-64bit
endif

# main packaging script based on dh7 syntax
%:
	dh $@  --no-parallel --with javahelper

# ant build needs to run first, do this later
override_dh_auto_configure:

override_dh_auto_clean:
	if [ -f "Makefile" ]; then make -j1 distclean; fi
	dh_auto_clean -- ${ant_flags}
	rm -f lib/perl/Nuxwdogclient/Makefile.real.old
	if [ -d "stash" ]; then mv stash/*.inc lib/perl/Nuxwdogclient; rmdir stash; fi

override_dh_auto_build:
	mkdir stash
	cp lib/perl/Nuxwdogclient/*.inc stash

	dh_auto_build -- ${ant_flags}

	# run this here	
	dh_auto_configure -- \
		${confflags}

override_dh_auto_install:
	dh_auto_install --destdir debian/tmp
	find debian/tmp -name '*.la' -exec rm -f {} ';'
	rm -f debian/tmp/usr/doc/LICENSE
	chmod -x debian/tmp/usr/jars/*
	mkdir debian/tmp/usr/share/java
	mv debian/tmp/usr/jars/nuxwdog.jar debian/tmp/usr/share/java/nuxwdog-${VERSION}.jar
	ln -s nuxwdog-${VERSION}.jar debian/tmp/usr/share/java/nuxwdog.jar
	mkdir -p debian/tmp/usr/lib/jni
	mv debian/tmp/usr/lib/${DEB_HOST_MULTIARCH}/libnuxwdog-jni.so \
		debian/tmp/usr/lib/jni


override_dh_install:
	dh_install --list-missing
	chrpath -d debian/nuxwdog/usr/lib/*/perl5/*/auto/Nuxwdogclient/Nuxwdogclient.so

override_dh_makeshlibs:
	dh_makeshlibs
	# silence lintian
	rm -f debian/libnuxwdog-java/DEBIAN/shlibs
